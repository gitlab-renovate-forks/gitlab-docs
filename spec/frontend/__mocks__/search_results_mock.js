export const mockResults = {
  results: [
    {
      title: 'Lorem ipsum dolor sit amet',
      link: 'https://example.com/1',
      headings: ['example heading'],
    },
    {
      title: 'Consectetur adipiscing elit',
      link: 'https://example.com/2',
      headings: ['example heading'],
    },
    {
      title: 'Sed do eiusmod tempor incididunt',
      link: 'https://example.com/3',
      headings: ['example heading'],
    },
    {
      title: 'Ut labore et dolore magna aliqua',
      link: 'https://example.com/4',
      headings: ['example heading'],
    },
    {
      title: 'Mauris blandit aliquet elit',
      link: 'https://example.com/5',
      headings: ['example heading'],
    },
    {
      title: 'Pellentesque euismod magna',
      link: 'https://example.com/6',
      headings: ['example heading'],
    },
    {
      title: 'Vestibulum ac diam sit amet quam',
      link: 'https://example.com/7',
      headings: ['example heading'],
    },
    {
      title: 'Cras ultricies ligula sed magna',
      link: 'https://example.com/8',
      headings: ['example heading'],
    },
    {
      title: 'Vivamus magna justo',
      link: 'https://example.com/9',
      headings: ['example heading'],
    },
    {
      title: 'Curabitur blandit tempus porttitor',
      link: 'https://example.com/10',
      headings: ['example heading'],
    },
  ],
  pagingStart: 1,
  pagingEnd: 10,
  totalResults: 10,
};

export const mockNoResults = {
  results: [],
};

export const mockErrorResults = {
  error: {
    code: 403,
    message: 'The request is missing a valid API key.',
    errors: [
      {
        message: 'The request is missing a valid API key.',
        domain: 'global',
        reason: 'forbidden',
      },
    ],
    status: 'PERMISSION_DENIED',
  },
};

export const mockHistoryCookie = [
  { path: '/runner/', title: 'GitLab Runner' },
  { path: '/ee/topics/plan_and_track.html', title: 'Plan and track work' },
  { path: '/ee/user/analytics/', title: 'Analyze GitLab usage' },
  { path: '/ee/user/infrastructure/', title: 'Infrastructure management' },
];
